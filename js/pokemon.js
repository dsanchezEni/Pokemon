/**
 * Classe permettant de définir un objet de type Attaque
 * Avec le libelle de l'attaque et le nombre de degats.
 */
class Attaque {
    constructor(libelle, degats) {
        this.libelle = libelle;
        this.degats = degats;
    }
}

/**
 * Classe qui représente un Objet Pokemon.
 * Avec le nom, le numéro, la taille,le poids; le type, les pvs (points de vie)
 * une image du Pokemon, une image correspondant à l'attaque 1
 * et une image correspondant à l'attaque 2.
 * Puis l'attaque1 et l'attaque2.
 */
class Pokemon {
    pvRestant;
    constructor(nom, numero, taille, poids, type, pv,imgPokemon,imgAttaque1,imgAttaque2,
                attaque1, attaque2) {
        this.nom = nom;
        this.numero = numero;
        this.taille = taille;
        this.poids = poids;
        this.type = type;
        this.pv = pv;
        this.imgPokemon = imgPokemon;
        this.imgAttaque1 = imgAttaque1;
        this.imgAttaque2 = imgAttaque2;
        this.attaque1 = attaque1;
        this.attaque2 = attaque2;
    }

    /**
     * Fonction permettant de réinitialiser les points de vies.
     */
    reinitialisationDesPv() {
        this.pvRestant = this.pv;
    }

    /**
     * Méthode en charge d'attaquer un Pokémon adverse.
     * Retourne un nombre de dégâts
     * @param Pokemon attaque l'autre Pokemon
     * @returns {number} correspond au dégat infligé.
     */
    attaque(Pokemon) {
        let attaqueUtilisee;
        let degats;
        // Si le Pokémon attaquant a plus de 20% des pv restants
        if (this.pvRestant > ((20 * this.pv) / 100)) {
            // Il utilise la première attaque
            attaqueUtilisee = this.attaque1;
            document.writeln("<p><img src=\"img/"+this.imgAttaque1+"\" alt=\""+this.nom+"\"/></p>");
        } else {
            // Sinon, il utilise la seconde
            attaqueUtilisee = this.attaque2;
            document.writeln("<p><img src=\"img/"+this.imgAttaque2+"\" alt=\""+this.nom+"\"/></p>");
        }
        // 10% de chances de doubler ses dégâts
        if (Math.random() < 0.1) {
            degats = attaqueUtilisee.degats * 2;
        } else {
            degats = attaqueUtilisee.degats
        }
        return degats;
    }
}

/**
 * Classe permettant de réaliser un combat entre les 2 Pokemons.
 */
class Combat {
    constructor(combattant01, combattant02) {
        let tirageAuSort = Math.random();
        if (tirageAuSort > 0.5) {
            // Soit le premier Pokémon passé en paramètre est le premier combattant
            this.combattant01 = combattant01;
            this.combattant02 = combattant02;
        } else {
            // Soit c'est l'inverse
            this.combattant01 = combattant02;
            this.combattant02 = combattant01;
        }
        document.writeln("<h4>" + combattant01.nom + " VS "
            + combattant02.nom + "</h4>");


        document.writeln("<div class=\"arene\">");
        document.writeln("<div class=\"combatant1\"><img src=\"img/"+this.combattant01.imgPokemon+"\" alt=\""+this.combattant01.nom+"\"/></div>");
        document.writeln("<div class=\"combatant2\"><img src=\"img/"+this.combattant02.imgPokemon+"\" alt=\""+this.combattant02.nom+"\"/></div>");
        document.writeln("</div>");

        document.writeln("<p>Le tirage au sort a décidé que " + this.combattant01.nom + " attaquait en premier.</p>");
        document.writeln("<p><img src=\"img/"+this.combattant01.imgPokemon+"\" alt=\""+this.combattant01.nom+"\"/></p>");
        document.writeln("<hr />");
    }

    /**
     * Fonction permettant de décider du Vainqueur entre 2 combattants.
     * @param combattant01 Correspond au premier Pokemon qui attaque.
     * @param combattant02 Correspond au deuxième Pokemon qui attaque.
     * @returns {null} soit null si égalité ou l'objet Pokemon vainqueur.
     */
    vainqueur(combattant01, combattant02) {
        let vainqueur;
        // Si un des deux Pokémon est KO, alors l'autre
        // est vainqueur
        if (combattant01.pvRestant > 0) {
            vainqueur = combattant01;
        } else if (combattant02.pvRestant > 0) {
            vainqueur = combattant02;
        }
        else if (combattant01.pvRestant < 0 && combattant02.pvRestant < 0) {
            vainqueur = null;
        }
        return vainqueur;
    }

    /**
     * Fonction permettant de lancer le combat dans l'arene.
     */
    combat() {
        // Réinitialisation des points de vie des deux
        // combattants
        this.combattant01.reinitialisationDesPv();
        this.combattant02.reinitialisationDesPv();
        document.writeln("<p>" + this.combattant01.nom + " a "
            + this.combattant01.pv + " points de vie.");
        document.writeln("<p>" + this.combattant02.nom + " a "
            + this.combattant02.pv + " points de vie.");
        document.writeln("<hr />");
        let degats;
        // Le combat continue tant que les deux combattants
        // ont des points de vie
        while (this.combattant01.pvRestant > 0 &&
        this.combattant02.pvRestant > 0) {
            // Le premier combattant attaque le deuxième
            degats = this.combattant01.attaque(this.combattant02);
            //document.writeln("<p><img src=\"img/"+this.combattant01.imgAttaque1+"\" alt=\""+this.combattant01.nom+"\"/></p>");
            // Les points de vie du deuxième combattant diminuent
            // selon le nombre de points de dégâts
            this.combattant02.pvRestant =
                this.combattant02.pvRestant - degats;
            document.writeln("<p>" + this.combattant01.nom + " a attaqué. Il a fait " + degats + " de dégâts. ");
            document.writeln("<p>" + this.combattant02.nom + " a " + this.combattant02.pvRestant + " de pvs. ");
            // Le deuxième combattant attaque le premier
            degats = this.combattant02.attaque(this.combattant01);
            //document.writeln("<p><img src=\"img/"+this.combattant02.imgAttaque1+"\" alt=\""+this.combattant02.nom+"\"/></p>");
            // Les points de vie du premier combattant diminuent selon le nombre de points de dégâts
            this.combattant01.pvRestant =
                this.combattant01.pvRestant - degats;
            document.writeln(this.combattant02.nom + " a attaqué.Il a fait " + degats + " de dégâts.</p>");
            document.writeln("<p>" + this.combattant01.nom + " a " + this.combattant01.pvRestant + " de pvs. ");
        }
        // À la fin du combat
        let vainqueur = this.vainqueur(this.combattant01, this.combattant02);
        // S'il existe un vainqueur, on le félicite
        if (vainqueur) {
            document.writeln("<h4>" + vainqueur.nom + " a gagné le combat. Il lui restait " + vainqueur.pvRestant + " pv.</h4>");
            document.writeln("<p><img src=\"img/"+vainqueur.imgPokemon+"\" alt=\""+vainqueur.nom+"\"/></p>");

        } else {
            // Sinon, il y a égalité
            document.writeln("<h4>EGALITE</h4>");
        }
    }
}

// Instanciation de Pikachu
pikachu = new Pokemon("Pikachu", 25, 60, 6.0, "Electrique", 82,
    "pikachu3.png","carte_pikachu_attaque_statik.jpg","pikachu_attaque_eclair1.gif",
    new Attaque("Statik", 10), new Attaque("ParaTonnerre", 25));

// Instanciation de Evoli
evoli = new Pokemon("Evoli", 133, 30, 6.5, "Normal", 70,"evoli.png",
    "evoli_attaque.jpg","evoli_attaque2.jpg",
    new Attaque("Adaptabilite", 9), new Attaque("Anticipation", 15));

//Création du combat.
let pikachuVSEvoli = new Combat(pikachu, evoli);
pikachuVSEvoli.combat();